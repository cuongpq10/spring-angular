package anphat.cuongpq.main.database.reponsitory

import anphat.cuongpq.main.database.model.NewsCategory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CategoryReponsitory : JpaRepository<NewsCategory, Int> {
    @Query(nativeQuery = true, value = "SELECT * FROM news_category WHERE title=:title ORDER BY id DESC")
    fun getCategoryByTitle(
            @Param(value = "title") title: String
    ): NewsCategory?

    @Query(nativeQuery = true, value = "SELECT * FROM news_category  ORDER BY id DESC")
    fun getAllCategory(): List<NewsCategory>?

    @Query(nativeQuery = true, value = "SELECT * FROM news_category WHERE id=:id ORDER BY id DESC")
    fun getCategoryById(
            @Param(value = "id") id: Int
    ): NewsCategory?

    @Query(nativeQuery = true, value = "SELECT * FROM news_category WHERE id=:id ORDER BY id DESC")
    fun deleteCategoryById(
            @Param(value = "id") id: Int
    ): NewsCategory?
}