package anphat.cuongpq.main.controller.admin.user

import anphat.cuongpq.main.database.model.UserAdmin
import anphat.cuongpq.main.model.request.LoginRequest
import anphat.cuongpq.main.model.response.BaseResponse
import anphat.cuongpq.main.model.response.LoginResponse

interface UserService {
    fun login(request : LoginRequest) : BaseResponse
    fun register(request : UserAdmin) : BaseResponse
    fun getUserByToken(token : String) : BaseResponse
}