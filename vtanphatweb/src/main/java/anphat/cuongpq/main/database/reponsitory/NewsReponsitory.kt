package anphat.cuongpq.main.database.reponsitory

import anphat.cuongpq.main.database.model.News
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface NewsReponsitory : JpaRepository<News,Int> {
    @Query(nativeQuery = true, value = "SELECT * FROM news ORDER BY id DESC LIMIT :limit")
    fun getAllNews(
            @Param(value = "limit") limit : Int
    ) : List<News>

    @Query(nativeQuery = true, value = "SELECT * FROM news WHERE title=:title ORDER BY id DESC LIMIT 1")
    fun getNewByTitle(
            @Param(value = "title") title : String
    ) : News?

    @Query(nativeQuery = true, value = "SELECT * FROM news WHERE id=:id ORDER BY id DESC LIMIT 1")
    fun getNewById(
            @Param(value = "id") id : Int
    ) : News?
}