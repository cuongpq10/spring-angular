package anphat.cuongpq.main.controller.admin.newscategory

import anphat.cuongpq.main.database.model.NewsCategory
import anphat.cuongpq.main.database.reponsitory.CategoryReponsitory
import anphat.cuongpq.main.model.response.BaseResponse
import anphat.cuongpq.main.utils.Utils
import io.jsonwebtoken.ExpiredJwtException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.rmi.AccessException

@Service
class DefaultCategoryService  : CategoryService{
    @Autowired
    private lateinit var categoryReponsitory: CategoryReponsitory

     override fun saveCategory(request: NewsCategory): BaseResponse {
         if (request.id != 0) {
             val category = categoryReponsitory.getCategoryById(request.id)
             if (category==null){
                 return BaseResponse("Danh mục không đã tồn tại",1)
             }
             category.description = request.description
             category.content = request.content
             category.image = request.image
             category.title = request.title
             categoryReponsitory.save(category)
         }else{
             val category = categoryReponsitory.getCategoryByTitle(request.title)
             if (category!=null){
                 return BaseResponse("Danh mục đã tồn tại",1)
             }
             categoryReponsitory.save(request)
         }
        return BaseResponse("Lưu danh mục thành công",0)
    }

    override fun getAllCategory(): BaseResponse {
        return BaseResponse(categoryReponsitory.getAllCategory())
    }

    override fun deleteCategory(id: Int): BaseResponse {
        var category = categoryReponsitory.getCategoryById(id)
        if (category==null){
            return BaseResponse("Xóa không thành công",1)
        }
        categoryReponsitory.delete(category)
        return BaseResponse("Xóa thành công",0)
    }

    override fun getCategoryById(id: Int): BaseResponse {
        return BaseResponse(
                categoryReponsitory.getCategoryById(id)
        )
    }
}