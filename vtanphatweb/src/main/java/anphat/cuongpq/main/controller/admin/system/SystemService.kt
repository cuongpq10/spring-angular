package anphat.cuongpq.main.controller.admin.system

import anphat.cuongpq.main.model.response.BaseResponse
import org.springframework.web.bind.annotation.RequestHeader

interface SystemService {
    fun checkToken(
            token : String
    ) : BaseResponse
}