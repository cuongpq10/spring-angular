package anphat.cuongpq.main.database.model

import javax.persistence.*

@Entity
@Table(name = "user_admin")
class UserAdmin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id = 0

    @Column(name = "username")
    var username: String = ""

    @Column(name = "passwords")
    var password: String = ""

    @Column(name = "dob")
    var dob: String = ""

    @Column(name = "fullname")
    var fullName: String = ""

    @Column(name = "avatar")
    var avatar: String = ""

    @Column(name = "time_update",insertable = false)
    var timeUpdate: String = ""
}