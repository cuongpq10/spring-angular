package anphat.cuongpq.main.database.reponsitory

import anphat.cuongpq.main.database.model.UserAdmin
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface AdminUserReponsitory : JpaRepository<UserAdmin, Int> {
    @Query(nativeQuery = true, value = "SELECT * FROM user_admin WHERE username = :username LIMIT 1")
    fun getUserAdminByUserName(
            @Param(value = "username") username: String
    ) : UserAdmin?

    @Query(nativeQuery = true, value = "SELECT * FROM user_admin WHERE id = :id LIMIT 1")
    fun getUserAdminById(
            @Param(value = "id") id: Int
    ) : UserAdmin?
}