package anphat.cuongpq.main.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
@Configuration

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Order(1)
open class WebSecurityConfig: WebSecurityConfigurerAdapter() {
    @Autowired
    lateinit var userService: UserService

    @Bean
    open fun jwtAuthenticationFilter() : JwtAuthenticationFilter{
        return JwtAuthenticationFilter()
    }
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    override fun authenticationManagerBean() : AuthenticationManager{
        return super.authenticationManagerBean()
    }
    @Bean
    open fun passwordEncoder(): PasswordEncoder{
        return BCryptPasswordEncoder()
    }

    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.userDetailsService(userService)
                ?.passwordEncoder(passwordEncoder())
        super.configure(auth)
    }


    override fun configure(http: HttpSecurity) {
        http
                .exceptionHandling()
                .and()
                .csrf()
                .disable()
                .headers()
                .frameOptions()
                .disable()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .authorizeRequests()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/api/register").permitAll()
                .antMatchers("/api/expired").permitAll()
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
        http.addFilterBefore(jwtAuthenticationFilter(),UsernamePasswordAuthenticationFilter::class.java)
    }
}