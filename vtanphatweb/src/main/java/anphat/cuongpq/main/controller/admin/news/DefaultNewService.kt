package anphat.cuongpq.main.controller.admin.news

import anphat.cuongpq.main.database.model.News
import anphat.cuongpq.main.database.reponsitory.NewsReponsitory
import anphat.cuongpq.main.model.response.BaseResponse
import anphat.cuongpq.main.utils.Utils
import io.jsonwebtoken.ExpiredJwtException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultNewService : NewService {
    @Autowired
    private lateinit var newsReponsitory : NewsReponsitory

    override fun getAllNews(): BaseResponse {
        return BaseResponse(
                newsReponsitory.getAllNews(5)
        )
    }

    override fun saveNew(request: News): BaseResponse {
        if (request.id != 0){
            var new = newsReponsitory.getNewById(request.id);
            if (new ==null){
                return BaseResponse("Cập nhập lỗi, bản tin không tồn tại",1)
            }
            new.title = request.title
            new.description = request.description
            new.content = request.content
            new.category = request.category
            new.image = request.image
            newsReponsitory.save(new)
            return BaseResponse("Chỉnh sửa tin thành công",0);
        }else{
            var new = newsReponsitory.getNewByTitle(request.title)
            if (new !=null){
                return BaseResponse("Tên bản tin đã tồn tại",1)
            }
            newsReponsitory.save(request)
            return BaseResponse("Thêm tin thành công",0)
        }
    }

    override fun deleteNew(id: Int): BaseResponse {
        var new = newsReponsitory.getNewById(id)
        if (new==null){
            return BaseResponse("Xóa thất bại, bản tin không tồn tại!",1)
        }
        newsReponsitory.delete(new)
        return BaseResponse("Xóa thành công", 0)
    }
}