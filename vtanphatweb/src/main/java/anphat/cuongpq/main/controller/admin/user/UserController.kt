package anphat.cuongpq.main.controller.admin.user

import anphat.cuongpq.main.database.model.UserAdmin
import anphat.cuongpq.main.model.request.LoginRequest
import anphat.cuongpq.main.model.response.BaseResponse
import anphat.cuongpq.main.model.response.LoginResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class UserController {
    @Autowired
    private lateinit var userService : UserService

    @PostMapping("/login")
    fun login(
           @Valid @RequestBody request: LoginRequest
    ) : BaseResponse {
        return userService.login(request)
    }
    @PostMapping("/register")
    fun register(
            @RequestBody request: UserAdmin
    ): BaseResponse{
        return userService.register(request)
    }
    @PostMapping("/profile")
    fun profile(
            @RequestHeader("Authorization") token: String
    ): BaseResponse{
        return userService.getUserByToken(token)
    }
}