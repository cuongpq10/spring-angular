package anphat.cuongpq.main.utils

import antlr.StringUtils
import io.jsonwebtoken.lang.Strings.hasText
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter : OncePerRequestFilter() {
    @Autowired
    lateinit var tokenProvider: JwtTokenProvider

    @Autowired
    lateinit var customUserDetailsService: UserService
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        try {
            var jwt = getJwtFromRequest(request)
            if (!jwt.equals("") && tokenProvider.validateToken(jwt!!)) {
                var userId = tokenProvider.getUserIdFromJWT(jwt)
                var userDetails = customUserDetailsService.loadUserById(userId)
                if (userDetails!=null){
                    var authentication : UsernamePasswordAuthenticationToken
                            = UsernamePasswordAuthenticationToken(userDetails,null,userDetails.authorities)
                    authentication.details=  WebAuthenticationDetailsSource().buildDetails(request)
                    SecurityContextHolder.getContext().setAuthentication(authentication)
                }
            }
        }catch (ex: Exception){
            println(ex)
        }
        filterChain.doFilter(request,response)
    }

    private fun getJwtFromRequest(request: HttpServletRequest): String? {
        var bearerToken = request.getHeader("Authorization")
        if (!bearerToken.equals("") && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7)
        }
        return null
    }
}