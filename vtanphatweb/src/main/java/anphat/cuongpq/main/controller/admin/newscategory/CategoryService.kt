package anphat.cuongpq.main.controller.admin.newscategory

import anphat.cuongpq.main.database.model.NewsCategory
import anphat.cuongpq.main.model.response.BaseResponse

interface CategoryService {
    fun saveCategory(request : NewsCategory): BaseResponse
    fun getAllCategory(): BaseResponse
    fun deleteCategory(id: Int): BaseResponse
    fun getCategoryById(id : Int) : BaseResponse
}