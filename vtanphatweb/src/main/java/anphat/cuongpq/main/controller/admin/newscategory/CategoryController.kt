package anphat.cuongpq.main.controller.admin.newscategory

import anphat.cuongpq.main.database.model.NewsCategory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/category")
class CategoryController {
    @Autowired
    private lateinit var categoryService: CategoryService

    @PostMapping("/save-category")
    fun saveCategory(
            @RequestBody() request: NewsCategory
    ): Any {
        return categoryService.saveCategory(request)
    }

    @GetMapping("/all-category")
    fun saveCategory(): Any {
        return categoryService.getAllCategory()
    }

    @DeleteMapping("/delete")
    fun deleteCategory(
            @RequestParam("idCategory") id: String
    ): Any {
        return categoryService.deleteCategory(Integer.parseInt(id))
    }

    @GetMapping("/{id}")
    fun getCategoryById(
            @RequestParam("idCategory") id: String
    ): Any {
        return categoryService.getCategoryById(Integer.parseInt(id))
    }
}