package anphat.cuongpq.main.controller.admin.news

import anphat.cuongpq.main.database.model.News
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/new")
class NewsController {
    @Autowired
    private lateinit var newService: NewService

    @GetMapping("/getAllNews")
    fun getAllNews(): Any {
        return newService.getAllNews()
    }

    @PostMapping("/save")
    fun saveNew(
            @RequestBody request: News
    ): Any {
        return newService.saveNew(request)
    }
    @DeleteMapping("/delete")
    fun deleteNew(
            @RequestParam id : Int
    ): Any {
        return newService.deleteNew(id)
    }
}