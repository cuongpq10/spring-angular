package anphat.cuongpq.main.utils

import anphat.cuongpq.main.database.model.UserAdmin
import anphat.cuongpq.main.database.reponsitory.AdminUserReponsitory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserService : UserDetailsService {
    @Autowired
    private lateinit var adminUserReponsitory: AdminUserReponsitory

    override fun loadUserByUsername(p0: String?): UserDetails {
        var user : UserAdmin? = p0?.let { adminUserReponsitory.getUserAdminByUserName(username = it) }
        if (user==null){
            throw UsernameNotFoundException(p0)
        }
        return CustomUserDetails(user)
    }
    fun loadUserById(userId : Int) :UserDetails?{
        var user = userId.let { adminUserReponsitory.getUserAdminById(userId) }
        if (user==null){
            throw UsernameNotFoundException(userId.toString())
        }
        return CustomUserDetails(user)
    }
}