package anphat.cuongpq.main.database.model

import javax.persistence.*

@Entity
@Table(name = "news")
class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Int = 0

    @Column(name = "title")
     var title: String=""

    @Column(name = "descriptions")
     var description: String=""

    @Column(name = "image")
     var image: String=""

    @Column(name = "content")
     var content: String=""

    @ManyToOne
    @JoinColumn(name = "category_id")
    lateinit var category: NewsCategory

    @Column(name = "time_update",insertable = false)
     var timeUpdate: String=""


}