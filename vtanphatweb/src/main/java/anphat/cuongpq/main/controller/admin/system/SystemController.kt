package anphat.cuongpq.main.controller.admin.system

import anphat.cuongpq.main.model.request.TokenRequest
import anphat.cuongpq.main.model.response.BaseResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.token.Token
import org.springframework.security.core.token.TokenService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController

@RestController
class SystemController {
    @Autowired
    private lateinit var systemService: SystemService

    @PostMapping("/api/expired")
    fun checkToken(
            @RequestBody request : TokenRequest
    ): Any? {
        return request.token?.let { systemService.checkToken(it) }
    }
}