package anphat.cuongpq.main.database.model

import org.springframework.format.annotation.DateTimeFormat
import javax.persistence.*

@Entity
@Table(name = "contact")
class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id=0
    @Column(name = "fullname")
    lateinit var fullName : String
    @Column(name = "email")
    lateinit var email : String
    @Column(name = "address")
    lateinit var address : String
    @Column(name = "phone")
    lateinit var phone : String
    @Column(name = "time_update",insertable = false)
    lateinit var timeUpdate : String
}