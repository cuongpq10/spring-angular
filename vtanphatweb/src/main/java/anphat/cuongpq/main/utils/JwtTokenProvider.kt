package anphat.cuongpq.main.utils

import io.jsonwebtoken.*
import org.springframework.stereotype.Component

import java.util.*

@Component
class JwtTokenProvider {
    private val JWT_SECRET = "CUONGPQ"
    private val JWT_EXPIRATION = 60480000L
    private val PREFIX = "Bearer "

    fun generateToken(userDetails: CustomUserDetails): String {
        var now = Date()
        var expiryDate = Date(now.time + JWT_EXPIRATION)
        return Jwts.builder()
                .setSubject(userDetails.user.id.toString())
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact()
    }

    fun getUserIdFromJWT(token: String): Int {
        var claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();
        return Integer.parseInt(claims.subject)
    }

    fun validateToken(token: String): Boolean {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(token)
            return true
        } catch (ex: MalformedJwtException) {
            println("Invalid JWT token")
        } catch (ex: ExpiredJwtException) {
            println("Expired JWT token")
        } catch (ex: UnsupportedJwtException) {
            println("Unsupported JWT token")
        } catch (ex: IllegalArgumentException) {
            println("JWT claims string is empty.")
        }
        return false
    }
    fun getTokenFromClient(token: String): String{
        return token.substring(PREFIX.length)
    }
}