package anphat.cuongpq.main.controller.admin.news

import anphat.cuongpq.main.database.model.News
import anphat.cuongpq.main.model.response.BaseResponse

interface NewService {
    fun getAllNews() : BaseResponse
    fun saveNew(request : News) : BaseResponse
    fun deleteNew(id: Int): BaseResponse
}