package anphat.cuongpq.main.controller.admin.user

import anphat.cuongpq.main.database.model.UserAdmin
import anphat.cuongpq.main.database.reponsitory.AdminUserReponsitory
import anphat.cuongpq.main.model.request.LoginRequest
import anphat.cuongpq.main.model.response.BaseResponse
import anphat.cuongpq.main.model.response.LoginResponse
import anphat.cuongpq.main.utils.CustomUserDetails
import anphat.cuongpq.main.utils.JwtTokenProvider
import anphat.cuongpq.main.utils.Utils
import io.jsonwebtoken.ExpiredJwtException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class DefaultUserService : UserService {
    @Autowired
    private lateinit var adminUserReponsitory: AdminUserReponsitory
    @Autowired
    private lateinit var authenticationManager: AuthenticationManager
    @Autowired
    private lateinit var tokenProvider: JwtTokenProvider

    override fun login(request: LoginRequest): BaseResponse {
        var authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        request.username,
                        request.password
                )
        )
        SecurityContextHolder.getContext().authentication = authentication
        var jwt =tokenProvider.generateToken(authentication.principal as CustomUserDetails)
        return BaseResponse(
                LoginResponse(
                        jwt
                )
        )
    }

    override fun register(request: UserAdmin): BaseResponse {
        var checkUser = adminUserReponsitory.getUserAdminByUserName(request.username)
        if (checkUser!=null){
            return BaseResponse("Username is exit",1)
        }
        request.password = BCryptPasswordEncoder().encode(request.password)
        adminUserReponsitory.save(request)
        return BaseResponse("Register is success",0)
    }

    override fun getUserByToken(token: String): BaseResponse {
        val userAdmin  = adminUserReponsitory.getUserAdminById(tokenProvider.getUserIdFromJWT(tokenProvider.getTokenFromClient(token)))
        if (userAdmin==null){
            return BaseResponse("Please login again!",-1)
        }
        return BaseResponse(
                userAdmin
        )
    }
}