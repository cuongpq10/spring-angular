package anphat.cuongpq.main.controller.admin.system

import anphat.cuongpq.main.model.response.BaseResponse
import anphat.cuongpq.main.utils.JwtTokenProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultSystemService : SystemService {
    @Autowired
    private lateinit var tokenProvider: JwtTokenProvider

    override fun checkToken(token: String): BaseResponse {
        return BaseResponse(
                tokenProvider.validateToken(tokenProvider.getTokenFromClient(token))
        )
    }
}