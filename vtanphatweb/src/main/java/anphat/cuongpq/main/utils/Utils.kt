package anphat.cuongpq.main.utils

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.util.*
import kotlin.collections.HashMap

object Utils {
    @JvmStatic
    fun getJWTFromInfoUser(id: Int,
                           username: String,
                           name: String): String {
        val claims = HashMap<String, Any>()
        claims.put("username", username)
        claims.put("name", name)
        claims.put("id", id)
        return Jwts.builder().setClaims(claims)
                .setSubject(id.toString())
                .setIssuedAt(Date())
                .signWith(SignatureAlgorithm.HS512, "CUONGPQ")
                .setExpiration(
                        Date(System.currentTimeMillis()
                                + 604800)
                )
                .compact()
    }

    @JvmStatic
    fun getUsernameFromToken(token: String): String {
        return Jwts.parser()
                .setSigningKey("CUONGPQ")
                .parseClaimsJws(token)
                .body.get("username", String::class.java)
    }

    @JvmStatic
    fun getIdFromToken(token: String): Int {
        return Jwts.parser()
                .setSigningKey("CUONGPQ")
                .parseClaimsJws(token)
                .body.subject.toInt()
    }

    @JvmStatic
    fun getExpirationDateFromToken(token: String): (Claims) -> Date {
        var claims = Jwts.parser().setSigningKey("CUONGPQ").parseClaimsJws(token).getBody()
        return Claims::getExpiration.apply { claims }
    }

    @JvmStatic
    fun isTokenExpired(token: String): Boolean {
        var expiration = getExpirationDateFromToken(token)
        if (expiration == Date()){
            return true
        }else{
            return false
        }

    }
}